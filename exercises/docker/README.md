# Docker

В этом задании мы упакуем готовое приложение в образ.

## Ссылки

* [Введение в Docker](https://www.youtube.com/watch?v=dfXuTTV6TVo)
* [Как и для чего использовать Docker](https://guides.hexlet.io/docker/)
* [Docker Overview](https://docs.docker.com/get-started/overview/)

## Задачи

* Инициализируйте вручную проект на [Fastify](https://github.com/fastify/fastify#quick-start)
* Упакуйте приложение в докер-контейнер. Для этого создайте *Dockerfile* с соответствующим содержимым
* Запустите контейнер и проверьте, что всё работает. При старте контейнера должен запускаться веб-сервер и быть доступен снаружи на 3000 порту
* Залейте образ на [Docker Hub](https://hub.docker.com/)
